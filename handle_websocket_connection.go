package main

import (
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// HandleWebSocket ...
func HandleWebSocket(w http.ResponseWriter, r *http.Request) {

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("internal server error"))
		return
	}

	client := Client{
		Conn: conn,
	}

	ps.AddClient(&client)

	subMsg := &Message{
		Event:   "ws_internal:connection_succeeded",
		Channel: "user-logs",
		Data:    map[string]interface{}{"message": "echo"},
	}

	client.Conn.WriteJSON(subMsg)

	defer client.Close()
	go ps.ReadMessage()

	for {
		messageType, payload, err := conn.ReadMessage()
		if err != nil {
			msg := Message{
				Event:   "ws:error",
				Channel: "user-logs",
				Data:    map[string]interface{}{"message": err.Error()},
			}

			client.Conn.WriteJSON(msg)
			return
		}

		ps.HandleWSMessage(&client, messageType, payload)
	}
}

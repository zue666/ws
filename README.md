# WebSocket Server

this is a sample code for a websocket server used to manage server-client communications.

>
> ** please note that some functionalies have been removed due to privacy reasons.
>

this code is responsible for register a new websocket client to the server, handling client removal, send client data to the client.

> missing functionalities:
> - authorization.
> - ping.
> - send metrics data.
> - MAY or MAY NOT missing something else.
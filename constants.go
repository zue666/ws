package main

var (
	// PUBLISH ...
	PUBLISH = "publish"

	// SUBSCRIBE ...
	SUBSCRIBE = "subscribe"

	// UNSUBSCRIBE ...
	UNSUBSCRIBE = "unsubscribe"

	// PING ...
	PING = "ping"

	// CHANNELSUBSCRIPTION ...
	CHANNELSUBSCRIPTION = "ws:subscribe"

	// USERUUID ...
	USERUUID = "ws:user_uuid"
)

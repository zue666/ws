package main

import "github.com/gorilla/websocket"

// Client ...
type Client struct {
	Conn *websocket.Conn
	UUID string `json:"user_uuid"`
}

// Clients ...
type Clients []*Client

// Close ...
func (c *Client) Close() {
	c.Conn.Close()
	ps.RemoveClient(c)
}

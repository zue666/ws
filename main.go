package main

import "net/http"

func main() {
	http.HandleFunc("/", HandleWebSocket)
	http.ListenAndServe(":8080", nil)
}

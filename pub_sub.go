package main

import (
	"encoding/json"
	"log"
)

var notificationCh = make(chan Message)

// PubSub ...
type PubSub struct {
	Clients  Clients
	Channels map[string]Clients
}

// AddClient to pubSub server
func (ps *PubSub) AddClient(c *Client) {
	ps.Clients = append(ps.Clients, c)
}

var ps = &PubSub{}

// HandleWSMessage ...
func (ps *PubSub) HandleWSMessage(c *Client, messageType int, payload []byte) *PubSub {
	msg := Message{}

	err := json.Unmarshal(payload, &msg)
	if err != nil {
		log.Println("Message payload in incorrect", err.Error())
	}

	switch msg.Event {
	case USERUUID:
		handleUserUUIDRegisteration(c, msg)
		break

	default:
		handleUserEvents(c, messageType, payload)
		break
	}

	return ps
}

// ReadMessage ...
func (ps *PubSub) ReadMessage() {
	for {
		mss := <-notificationCh
		if mss.Event == "ws:data-received" {
			sendMessageReceivedNotification(mss)
			return
		}

		for _, client := range ps.Clients {
			message := Message{
				Event: "ws_internal:notification",
				Data:  mss.Data,
			}
			client.Conn.WriteJSON(message)
		}
	}
}

func sendMessageReceivedNotification(msg Message) {
	for _, client := range ps.Clients {
		err := client.Conn.WriteJSON(msg)
		if err != nil {
			log.Println("Error:", err)
		}
	}
}

// RemoveClient ...
func (ps *PubSub) RemoveClient(c *Client) *PubSub {
	ps.Clients = append(ps.Clients[:ps.clientIndex(c)], ps.Clients[ps.clientIndex(c):]...)
	return ps
}

func (ps *PubSub) clientIndex(c *Client) int {
	for i, client := range ps.Clients {
		if client == c {
			return i
		}
	}
	return -1
}
